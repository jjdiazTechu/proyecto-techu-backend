var express = require('express');
var usersFile = require('./users.json')
var cuentasFile = require('./cuentas.json')
var bodyParser = require('body-parser')
var app = express();
app.use(bodyParser.json());
var port = process.env.PORT || 3000;
const URI = '/api-uruguay/v1/';


// GET users
app.get(URI + 'users',
  function(req, res) {
    //res.send('Hola API desde NODEmon');
    //res.sendFile('users.json', {root: __dirname});
    console.log('GET /users');
    res.send(usersFile);
} );


// GET users con ID
app.get(URI + 'users/:id',
  function(req, res) {

    console.log('GET /users/Id');
    let idUser = req.params.id;
    res.send(usersFile[idUser - 1]);
} );


// GET cuentas
app.get(URI + 'cuentas',
  function(req, res) {
    //res.send('Hola API desde NODEmon');
    //res.sendFile('cuentas.json', {root: __dirname});
    //console.log(users.json);

    console.log('GET /cuentas');
    res.send(cuentasFile);
} );


// GET cuentas con ID
app.get(URI + 'cuentas/:id',
  function(req, res) {

    console.log('GET /cuentas/Id');
    let idCuenta = req.params.id;
    res.send(cuentasFile[idCuenta - 1]);

} );


// POST users datos en body
app.post(URI + 'users',
  function(req, res) {

    console.log('POST users');
    console.log(req.body);
    let tam = usersFile.length + 1;

    //var userNuevo = req.body;
    var userNuevo = {
      'id' : tam,
      'first_name' : req.body.first_name,
      'last_name' : req.body.last_name,
      'email' : req.body.email,
      'password' : req.body.password
    };

  usersFile.push(userNuevo);
  res.send({'message': 'POST realizado correctamente', userNuevo});

  //res.send({'message': 'POST realizado correctamente'});
  //var msg = {'message': 'POST realizado correctamente'};
  //res.send(msg);

} );


// POST users datos en header
app.post(URI + 'users-post',
  function(req, res) {

    console.log('POST users-post');
    console.log(req.headers);
    let tam = usersFile.length + 1;

    var userNuevo = {
      'id' : tam,
      'first_name' : req.headers.first_name,
      'last_name' : req.headers.last_name,
      'email' : req.headers.email,
      'password' : req.headers.password
    };

    usersFile.push(userNuevo);
    res.send({'message': 'POST header realizado correctamente', userNuevo});

} );


// PUT users
app.put(URI + 'users/:id',
  function(req, res) {

    console.log('PUT /users');
    let idUpdate = req.params.id;
    var existe = true;

    if(usersFile[idUpdate - 1] == undefined){
      console.log('Usuario no existe')
      existe = false;
    }
    else {

      let userUpdate = {
        'id' : idUpdate,
        'first_name' : req.body.first_name,
        'last_name' : req.body.last_name,
        'email' : req.body.email,
        'password' : req.body.password
      }
      usersFile[idUpdate - 1] = userUpdate;
    }

    var msgResponse = existe ? {'msg' : 'Update Ok'} : {'msg' : 'Update ERR'};
    var statusCode  = existe ? 200 : 400;
    res.status(statusCode).send(msgResponse);
} );

// DELETE users
app.delete(URI + 'users/:id',
  function(req, res) {

    console.log('DELETE /users');
    let idDelete = req.params.id;
    var existe = true;

    if(usersFile[idDelete - 1] == undefined){
      console.log('Usuario no existe')
      existe = false;
    }
    else {

      //elimina desde la posicion x elementos
      usersFile.splice(idDelete - 1, 1);
    }

    var msgResponse = existe ? {'msg' : 'Delete Ok'} : {'msg' : 'Delete ERR'};
    var statusCode  = existe ? 200 : 400;
    res.status(statusCode).send(msgResponse);
} );


// GET users con query string
//app.get(URI + "'users?qname='Ezequiel'&qlast_name='Llarena'",
app.get(URI + 'usersqry',

  function(req, res) {

    console.log('GET /users query string');

    //usersFile.find();

    let encontrado  = false;
    let msgResponse = {'msg' : 'Usuario NO encontrado'};

    //console.log(req.query.qname);
    //console.log(req.query.qlast_name);

    for(i = 0 ; i < usersFile.length && !encontrado ; ++i){
      //console.log(usersFile[i].first_name);
      //console.log(usersFile[i].last_name);

      if (usersFile[i].first_name == req.query.qname && usersFile[i].last_name == req.query.qlast_name){
        msgResponse = {'msg' : 'Usuario encontrado'};
        encontrado = true;
      }
    }

    var statusCode  = encontrado ? 200 : 400;
    res.status(statusCode).send(msgResponse);

} );


app.listen(port);
console.log('Escuchando en el puerto 3000');
