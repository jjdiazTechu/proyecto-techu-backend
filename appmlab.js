var express = require('express');
var usersFile = require('./users.json')
var cuentasFile = require('./cuentas.json')
var bodyParser = require('body-parser')
var requestJSON = require('request-json')
var app = express();
app.use(bodyParser.json());
var port = process.env.PORT || 3000;
const URI = '/api-uruguay/v1/';


var newID = 0;
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techubduruguayjj/collections/';
var apikeyMLab = 'apiKey=4yA6GaDpueRgS5sL0A9SPYjCOD8BPwHa';


// GET users con API REST de MLab
app.get(URI + 'users',
  function(req, res) {

    console.log('MLab GET /users');

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");

    var queryString = 'f={"_id":0}&';

    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};

        if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
        } else {
            if(body.length > 0) {
                response = body;
            } else {
                response = {"msg" : "usuario no encontrado."}
                res.status(404);
            }
        }

        res.send(response);
      });

} );


// GET users con ID con API REST de MLab
app.get(URI + 'users/:id',
  function(req, res) {

    console.log('MLAB GET /users/Id');
    console.log(req.params.id);

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");

    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';

    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};

        if(err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
        } else {
            if(body.length > 0) {
                response = body;
            } else {
                response = {"msg" : "usuario no encontrado."}
                res.status(404);
            }
        }

        res.send(response);
      });

} );


// POST users con API REST de MLab
app.post(URI + 'users',
  function(req, res) {

    console.log('MLAB POST users-post');

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");

    //el GET lo hace para obtener el id para el insert
    httpClient.get('user?' + apikeyMLab,
      function(err, respuestaMLab, body) {

        newID = body.length + 1;
        console.log("newID:" + newID);

        var newUser = {
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };

        httpClient.post(baseMLabURL + "user?" + apikeyMLab, newUser,
          function(err, respuestaMLab, body){
            res.send(body);
        });
    });

} );


// PUT users con API REST de MLab
app.put(URI + 'users/:id',
    function(req, res) {

      console.log('MLAB PUT /users');
      console.log(req.params.id);

      var httpClient = requestJSON.createClient(baseMLabURL);

      console.log("Cliente HTTP mLab creado.");

      var id = req.params.id;
      var queryString = 'q={"id":' + id + '}&';

      httpClient.get('user?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body) {

          var response = {};

          if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
          } else {
              if(body.length > 0) {

                var cambio = '{"$set":' + JSON.stringify(req.body) + '}';

                httpClient.put(baseMLabURL + 'user?q={"id": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
                  function(err, respuestaMLab, body) {

                  console.log("body:"+ body);
                  response = body;
                 });

              } else {
                  response = {"msg" : "usuario no encontrado."}
                  res.status(404);
              }
          }

          res.send(response);
        });

    } );


// DELETE users con API REST de MLab
app.delete(URI + 'users/:id',
  function(req, res) {

    console.log('MLAB DELETE /users');
    console.log("request.params.id: " + req.params.id);

    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");

    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';

    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {

        var respuesta = body[0];
        console.log("body delete:"+ respuesta);

        httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
          function(err, respuestaMLab, body){
            res.send(body);
          });
      });
  });


  app.listen(port);
  console.log('Escuchando en el puerto 3000');
